import React from "react";
import {Link} from "react-router-dom";

export class PageThree extends React.Component {
    render() {
        return(
            <div>
                <h1>This is page 3!</h1>
                <div>
                    <ul>
                        <li>
                            <Link to={"/"}>Home</Link>
                        </li>
                        <li>
                            <Link to={"/page1"}>Page One</Link>
                        </li>
                        <li>
                            <Link to={"/page2"}>Page 2</Link>
                        </li>
                        <li>
                            <Link to={"/page3"}>Page 3</Link>
                        </li>
                    </ul>
                </div>
            </div>
        )
    }
}
