import React from 'react';
import { Route } from 'react-router-dom'
import {HomePage} from "./pages/HomePage";
import {PageOne} from "./pages/PageOne";
import {PageTwo} from "./pages/PageTwo";
import {PageThree} from "./pages/PageThree";
import {Switch} from "react-router";

function App() {
    return (
        <div>
            <Switch>
            <Route path={"/page1"} component={PageOne} />
            <Route path={"/page2"} component={PageTwo} />
            <Route path={"/page3"} component={PageThree} />
            <Route exact path={"/"} component={HomePage}/>
            </Switch>
        </div>
    );
}

export default App;
